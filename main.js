export function setup(ctx) {
    const BREAKDOWN_PAGE = 4676;

    const htmlID = (id) => id.replace(/[:_]/g, '-').toLowerCase();

    //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    // Citizen Page - It gets rebuilt every time it updates. So have to rebuild our things as well.
    const CITIZEN_TYPES = [
        {
            'id': 'baby',
            'label': 'Baby',
            'range': [0, game.township.MIN_WORKER_AGE]
        },
        {
            'id': 'worker',
            'label': 'Worker',
            'range': [game.township.MIN_WORKER_AGE, game.township.AGE_OF_DEATH]
        },
        {
            'id': 'potions',
            'label': 'Worker <img src="assets/media/skills/township/potion.svg" title="Require Potions">',
            'range': [game.township.AGE_OF_DEATH, game.township.MAX_WORKER_AGE + 1]
        },
        {
            'id': 'old',
            'label': 'Retired <img src="assets/media/skills/township/potion.svg" title="Require Potions">',
            'range': [game.township.MAX_WORKER_AGE, 99]
        }
    ];

    const JOB_COLORS = {
        "melvorD:Unemployed": '#FF6384',
        "melvorF:Blacksmith": '#FF9F40',
        "melvorF:Fisherman": '#FFCD56',
        "melvorF:Woodcutter": '#4BC0C0',
        "melvorF:Stone_Miner": '#36A2EB',
        "melvorF:Ore_Miner": '#9966FF',
        "melvorF:Coal_Miner": '#C9CBCF',
        "melvorF:Gatherer": '#CCC876',
        "melvorF:Rune_Essence_Miner": '#AC94F9',
        "melvorF:Crafter": '#605D23',
        "melvorF:Apothecarist": '#856A92',
        "melvorF:Plank_Maker": '#9AA8CE',
        "melvorF:Tailor": '#5B6786',
    };

    const getCitizenType = (age) => {
        if(age < game.township.MIN_WORKER_AGE) return 'baby';
        if(age > game.township.MAX_WORKER_AGE) return 'old';
        if(age >= game.township.AGE_OF_DEATH) return 'potions';
        return 'worker';
    };

    const citizenLegendNode = (group) => {
        const width = (group.range[1] - group.range[0]);
        return `<div style="width:${width}%;">
                <div class="legend ${group.id}">
                    ${group.label}
                </div>
            </div>`;
    };

    const citizenTotalNode = (group, buckets, total) => {
        let totalInGroup = 0;
        for (let a = group.range[0]; a <= group.range[1]; a++) {
            totalInGroup += buckets[a].qty;
        }
        const percent = (total == 0 ? 100 : Math.min(100, Math.max(0, (totalInGroup / total) * 100)));
        const width = (group.range[1] - group.range[0]);
        return `<div style="width:${width}%;">
                <div class="legend ${group.id}">
                    <small>${totalInGroup} / ${total} (${percent.toFixed(0)}%)</small>
                </div>
            </div>`;
    };

    const citizenGraphLabels = (step, max) => {
        let labelhtml = '';
        for(let l = 0; l < step; l++) {
            labelhtml += `<div style="top:${(l * (100 / step))}%;">${Math.round((step - l) * (max / step))}</div>`;
        }
        return labelhtml;
    };

    const buildCitizenGraph = () => {
        const roundDownToNearest = (step, number) => Math.floor(number / step) * step;
        const roundUpToNearest = (step, number) => Math.ceil(number / step) * step;
        const bucketHighest = (arr, key) => arr.reduce((acc, curr) => curr[key] > acc ? curr[key] : acc, 0);
        const bucketTotal = (arr, key) => arr.reduce((acc, curr) => curr[key] + acc, 0);

        const buildBuckets = (step) => {
            const buckets = [];
            for (let i = 0; i < (100 / step); i++) {
                buckets[i] = {qty: 0, jobs: new Map(), workers: 0, unemployed: 0, nowork: 0};
            }

            game.township.citizens.forEach(c => {
                const age = game.township.getCitizenAge(c);
                const agestep = roundDownToNearest(step, age) / step;
                const job = c.job;
                const bucket = buckets[agestep];

                if (bucket == null)
                    return;

                bucket.qty++;
                bucket.jobs.set(job, (bucket.jobs.get(job) || 0) + 1);
                if (age >= game.township.MIN_WORKER_AGE && age <= game.township.MAX_WORKER_AGE) {
                    if (job != game.township.unemployedJob) {
                        bucket.workers++;
                    }
                    else {
                        bucket.unemployed++;
                    }
                }
                else {
                    bucket.nowork++;
                }
            });
            return buckets;
        };

        const JOB_RANGE = 5;

        const populationData = buildBuckets(1);
        const jobData = buildBuckets(JOB_RANGE);

        const totalPopulation = game.township.citizens.length;
        const mostOfAge = roundUpToNearest(10, bucketHighest(populationData, 'qty'));
        const mostOfWorker = roundUpToNearest(10, bucketHighest(populationData, 'workers'));

        //console.log(`Total Population:`, totalPopulation, `Highest Age Population:`, mostOfAge, `Highest Worker Count:`, mostOfWorker);
        //console.log(populationData);
        //console.log(jobData);

        const populationGraph = populationData.map((bucket, i) => {
            const count = populationData[i].qty;
            return `<div class="${getCitizenType(i)}" style="height:${Math.round((count / mostOfAge)*100)}%" data-tippy-content="Age ${i}: ${count}"></div>`;
        }).join('');

        const jobGraph = game.township.jobs.reduce((jobs, job) => {
            const jobColor = JOB_COLORS[job.id];
            jobs.push({
                'label': job.name,
                'backgroundColor': jobColor,
                'borderColor': jobColor,
                'color': 'rgb(255,255,255)',
                'data': jobData.reduce((group, ageRange, ageIdx) => {
                    const jobsAge = ageRange.jobs.get(job);
                    if (jobsAge != null) {
                        group.push({
                            'age': ageIdx * JOB_RANGE,
                            'job': job.name,
                            'count': jobsAge
                        });
                    }
                    return group;
                }, [])
            });
            return jobs;
        }, []);

        // Add HTML
        const node = $(`
        <div class="citizen-legend row no-gutters mt-4 pt-4 font-size-sm border-top border-crafting">
            ${CITIZEN_TYPES.map(type => citizenLegendNode(type)).join('')}
        </div>
        <div class="citizen-age-graph mt-2">
            <div class="bars"><div style="top:20%;"></div><div style="top:40%;"></div><div style="top:60%;"></div><div style="top:80%;"></div></div>
            <div class="graph" style="grid-template-columns: repeat(100, 1fr);">${populationGraph}</div>
            <div class="text count-labels">${citizenGraphLabels(5, mostOfAge)}</div>
        </div>
        <div class="citizen-legend row no-gutters mt-2 font-size-sm">
            ${CITIZEN_TYPES.map(type => citizenTotalNode(type, populationData, totalPopulation)).join('')}
        </div>
        <div class="row no-gutters mt-4 pt-4 font-size-sm border-top border-crafting">
            <div style="width:100%;height:400px;">
                <canvas id="populationJobGraph"></canvas>
            </div>
        </div>
        `);

        $(townshipUI.defaultElements.div.citizens).append(node);

        // Add Age Tooltips
        tippy(document.querySelectorAll('.citizen-age-graph [data-tippy-content]'), {
            animation: false,
            allowHTML: true
        });

        // Display Job Chart
        new Chart(document.getElementById('populationJobGraph').getContext('2d'), {
            type: 'bar',
            data: {
                labels: jobData.map((data, idx) => idx * JOB_RANGE),
                datasets: jobGraph
            },
            options: {
                plugins: {
                    title: {
                        display: true,
                        text: 'Job Breakdown by Age',
                        color: "#FFFFFF"
                    },
                    legend: {
                        labels: {
                            color: "#FFFFFF"
                        }
                    }
                },
                parsing: {
                    xAxisKey: 'age',
                    yAxisKey: 'count'
                },
                scales: {
                    x: {
                        grid: {
                            color: "#404650"
                        },
                        ticks: {
                            color: "#FFFFFF",
                            stepSize: JOB_RANGE,
                            beginAtZero: true
                        },
                        stacked: true,
                    },
                    y: {
                        grid: {
                            color: "#404650"
                        },
                        ticks: {
                            color: "#FFFFFF",
                            stepSize: 5,
                            beginAtZero: true
                        },
                        stacked: true,
                    }
                },
                animation: false,
                responsive: true,
                maintainAspectRatio: false
            }
        });
    };

    //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    // Breakdown Page
    const BIOME_ICONS = {
        'melvorF:Grasslands': 'assets/media/skills/combat/goblin_village.svg',
        'melvorF:Forest': 'assets/media/shop/woodcutting_multi_tree.svg',
        'melvorF:Desert': 'assets/media/skills/combat/desolate_plains.svg',
        'melvorF:Water': 'assets/media/skills/combat/dark_waters.svg',
        'melvorF:Swamp': 'assets/media/skills/combat/toxic_swamps.svg',
        'melvorF:Arid_Plains': 'assets/media/skills/combat/arid_plains.svg',
        'melvorF:Mountains': 'assets/media/skills/combat/perilous_peaks.svg',
        'melvorF:Valley': 'assets/media/skills/combat/dragon_valley.svg',
        'melvorF:Jungle': 'assets/media/skills/combat/wet_forest.svg',
        'melvorF:Snowlands': 'assets/media/skills/combat/icy_hills.svg'
    };
    const BIOME_SHORT = {
        'melvorF:Grasslands': 'GL',
        'melvorF:Forest': 'FS',
        'melvorF:Desert': 'DS',
        'melvorF:Water': 'WR',
        'melvorF:Swamp': 'SW',
        'melvorF:Arid_Plains': 'AP',
        'melvorF:Mountains': 'MT',
        'melvorF:Valley': 'VY',
        'melvorF:Jungle': 'JG',
        'melvorF:Snowlands': 'SL'
    };

    const MODIFIER_DATA = [
        ["PopulationCap", "Population Cap", ""],
        ["DeadStorage", "Dead Storage", "%"],
        ["Education", "Education", "%"],
        ["MaxStorage", "Max Storage", "%"],
        ["Happiness", "Happiness", "%"],
        ["Health", "Health", "%"],
        ["TaxPerCitizen", "Tax Per Citizen", "%"],
        ["BuildingHappinessPenalties", "Building Happiness Penalties", "%"],
        ["CoalUsage", "Coal Usage", "%"],
        ["FoodUsage", "Food Usage", "%"],
        ["BuildingCost", "Building Cost", "%"],
        ["GrasslandsProduction", "Grasslands Production", "%"],
        ["ForestProduction", "Forest Production", "%"],
        ["DesertProduction", "Desert Production", "%"],
        ["WaterProduction", "Water Production", "%"],
        ["SwampProduction", "Swamp Production", "%"],
        ["AridPlainsProduction", "Arid Plains Production", "%"],
        ["MountainsProduction", "Mountains Production", "%"],
        ["ValleyProduction", "Valley Production", "%"],
        ["JungleProduction", "Jungle Production", "%"],
        ["SnowlandsProduction", "Snowlands Production", "%"],
        ["GPProduction", "GP Production", "%"],
        ["ResourceProduction", "Resource Production", "%"],
        ["FoodProduction", "Food Production", "%"],
        ["WoodProduction", "Wood Production", "%"],
        ["OreProduction", "Ore Production", "%"],
        ["StoneProduction", "Stone Production", "%"],
        ["CoalProduction", "Coal Production", "%"],
        ["BarProduction", "Bar Production", "%"],
        ["HerbProduction", "Herb Production", "%"],
        ["RuneEssenceProduction", "Rune Essence Production", "%"],
        ["LeatherProduction", "Leather Production", "%"],
        ["PotionProduction", "Potion Production", "%"],
        ["PlankProduction", "Plank Production", "%"],
        ["ClothingProduction", "Clothing Production", "%"],
        ["FishingDockProduction", "Fishing Dock Production", "%"],
        ["MagicEmporiumProduction", "Magic Emporium Production", "%"],
        ["OrchardProduction", "Orchard Production", "%"],
        ["FarmProduction", "Farm Production", "%"],
        ["WoodcuttingProduction", "Woodcutting Production", "%"],
        ["BlacksmithProduction", "Blacksmith Production", "%"]
    ];

    const getMap = () => {
        return game.township.maps.find(map => game.township.biomes.every(biome => biome.totalInMap == map.biomeCounts.get(biome)));
    };

    const getTotalMapSize = () => {
        return game.township.biomes.reduce((currentValue, biome) => currentValue+biome.totalInMap, 0);
    };

    const getBuildingCount = () => {
        return game.township.biomes.reduce((currentValue, biome) => currentValue + getBuildingCountBiome(biome), 0);
    };

    const getBuildingCountBiome = (biome) => {
        let buildingSum = 0;
        biome.buildingsBuilt.forEach(build => {
            buildingSum += build;
        });
        return buildingSum;
    }

    const getBiomeNode = (biome) => {
        const biomeBuildings = getBuildingCountBiome(biome);
        const netClass = (biome.totalInMap == biome.amountPurchased && biome.availableInMap == 0 ? 'text-success' : '');
        return `<div class="biome-list border-${ htmlID(biome.id) }"><img src="${ BIOME_ICONS[biome.id] }" class="skill-icon-xs rounded-circle pl-1">${biome.name}<div class="float-right pt-1 ${ netClass }">${ Math.max(biome.totalInMap, biomeBuildings) - biome.availableInMap } / ${ biome.totalInMap }</div></div>`;
    };

    const getBiomeList = () => {
        const map = getMap();
        const mapName = map ? map.name : 'Map: Unknown / Custom';

        const maxBuilds = getTotalMapSize();
        const builtBuilds = getBuildingCount();

        return `${game.township.biomes.allObjects.map(biome => getBiomeNode(biome)).join('')}
            <div class="border-top border-crafting mt-4 pt-4">
                <div class="text-warning font-size-base">${ mapName }</div>
                <div>Land<div class="float-right">${ maxBuilds }</div></div>
                <div>Used<div class="float-right">${ builtBuilds }</div></div>
                <div>Purchased<div class="float-right">${ game.township.townData.sectionsPurchased }</div></div>
            </div>
        `;
    };

    const getTownStatus = () => {
        const displayValues = (value, max, isGood = true) => {
            const percent = (max == 0 ? 100 : Math.min(100, Math.max(0, (value / max) * 100)));
            const textClass = percent > 98.5 ? (isGood ? 'text-success' : 'text-danger') : 'text-muted';
            return `<small class="${ textClass }">${ numberWithCommas(value) } / ${ numberWithCommas(max) } (${ percent.toFixed(0) }%)</small>`;
        };
        const displayValuesPercent = (value, isGood = true) => {
            const textClass = value > 98.5 ? (isGood ? 'text-success' : 'text-danger') : 'text-muted';
            return `<small class="${ textClass }">(${ value.toFixed(0) }%)</small>`;
        };

        return `
            <div class="border-top border-crafting mt-4 pt-4">
                <div>Population<div class="float-right">${ displayValues(game.township.currentPopulation, game.township.populationLimit) }</div></div>
                <div>Dead<div class="float-right">${ displayValues(game.township.totalDead, game.township.availableDeadStorage, false) }</div></div>
                <div>Storage<div class="float-right">${ displayValues(Math.floor(game.township.getUsedStorage()), game.township.getMaxStorage(), false) }</div></div>
                <div>Happiness<div class="float-right">${ displayValues(game.township.townData.happiness, game.township.maxHappiness) }</div></div>
                <div>Education<div class="float-right">${ displayValues(game.township.townData.education, game.township.maxEducation) }</div></div>
                <div>Health<div class="float-right">${ displayValuesPercent(game.township.townData.healthPercent) }</div></div>
                <div>${ game.township.currentWorshipName }<div class="float-right">${ displayValues(game.township.townData.worshipCount, game.township.MAX_WORSHIP) }</div></div>
            </div>`;
    };

    const getBuildingNode = (biome) => {
        let buildings = '';
        biome.buildingsBuilt.forEach((amount, building) => {
            buildings += `<div class="mr-2 mb-2 p-1 float-left text-center building-item border-${ htmlID(biome.id) }">
                <div class="position-relative">
                    <div class="building-biome-icon">
                        <img src="${ BIOME_ICONS[biome.id] }" class="skill-icon-xs rounded-circle" alt="${ BIOME_SHORT[biome.id]} ">
                    </div>
                    <div>
                        <img src="${ building.media }" class="resize-48" />
                    </div>
                    <div>
                        <small class="text-warning text-nowrap">${ building.name }</small>
                    </div>
                    <div>
                        <small class="badge badge-primary">${ amount }</small>
                    </div>
                </div>
            </div>`;
        });
        return buildings;
    };

    const getBuildingList = () => {
        return game.township.biomes.allObjects.map(biome => getBuildingNode(biome)).join('');
    };

    const getModifierNode = (modData) => {
        return `<div class="pb-2 mb-2 font-size-sm border-bottom border-smithing">
            <div class="row no-gutters pt-1">
                <div class="col-6 text-left">${modData[1]}</div>
                <div class="col-3 text-success text-right">+${game.modifiers['increasedTownship' + modData[0]]}${modData[2]}</div>
                <div class="col-3 text-danger text-right">-${game.modifiers['decreasedTownship' + modData[0]]}${modData[2]}</div>
            </div>
        </div>`;
    };

    const showModifierData = () => {
        const modhtml = MODIFIER_DATA.map(mod => getModifierNode(mod)).join('');
        SwalLocale.fire({
            title: `Modifier Data`,
            html: modhtml
        });
    };

    const getTotalResourceNode = () => {
        const resources = game.township.resources.allObjects;
        const netRate = resources.reduce((acc, resource) => acc + game.township.getNetResourceRate(resource), 0);
        const netText = `${ netRate > 0 ? '+' : '' }${ numberWithCommas(netRate.toFixed(2)) } /t`;
        const netClass = (netRate > 0 ? 'success' : (netRate < 0 ? 'danger' : 'muted'));

        const trueGain = resources.reduce((acc, resource) => acc + (resource.type === TownshipResourceTypeID.Product ? game.township.getTrueMaxProductCreationAmount(resource) : resource.generation), 0);
        const trueUsage = resources.reduce((acc, resource) => acc + game.township.getTrueResourceUsage(resource), 0);

        const storageTicks = Math.round((game.township.getMaxStorage() - game.township.getUsedStorage()) / netRate);

        return `<li class="resource-item mb-2 pl-2 pr-2 w-100 border-${ netClass }">
            <div class="media d-flex align-items-center">
                <div class="mr-1">
                    <i class="fa fa-box-open mr-1 pt-2 pb-2 font-size-base"></i>
                </div>
                <div class="media-body">
                    <small class="font-w600">${ numberWithCommas(game.township.getUsedStorage()) }</small>
                </div>
                <div class="media-body text-right">
                    <small><span class="text-${ netClass }">${ netText }</span></small>
                </div>
            </div>
            <div class="border-top border-smithing w-100 row no-gutters">
                <div class="col-6"><small class="text-success">+${ numberWithCommas(trueGain.toFixed(2)) }</small></div>
                <div class="col-6 text-right"><small class="text-danger">-${ numberWithCommas(trueUsage.toFixed(2)) }</small></div>
            </div>
            <div class="w-100">
                <small>Storage Overflow in ${ numberWithCommas(storageTicks) } ticks</small>
            </div>
        </li>`;
    };

    const getResourceNode = (resource) => {
        const netRate = game.township.getNetResourceRate(resource);
        const netText = `${ netRate > 0 ? '+' : '' }${ numberWithCommas(netRate.toFixed(2)) } /t`;
        const netClass = (netRate > 0 ? 'success' : (netRate < 0 ? 'danger' : 'muted'));

        const trueGain = resource.type === TownshipResourceTypeID.Product ? game.township.getTrueMaxProductCreationAmount(resource) : resource.generation;
        const trueUsage = game.township.getTrueResourceUsage(resource);

        return `<li class="resource-item mb-2 pl-2 pr-2 w-100 border-${ netClass }">
            <div class="media d-flex align-items-center">
                <div class="mr-1">
                    <img class="skill-icon-xs mr-1" src="${ resource.media }">
                </div>
                <div class="media-body">
                    <small class="font-w600">${ numberWithCommas(Math.floor(resource.amount)) }</small>
                </div>
                <div class="media-body text-right">
                    <small><span class="text-${ netClass }">${ netText }</span></small>
                </div>
            </div>
            <div class="border-top border-smithing w-100 row no-gutters">
                <div class="col-6"><small class="text-success">+${ numberWithCommas(trueGain.toFixed(2)) }</small></div>
                <div class="col-6 text-right"><small class="text-danger">-${ numberWithCommas(trueUsage.toFixed(2)) }</small></div>
            </div>
        </li>`;
    };

    const getResourceList = () => {
        return `${getTotalResourceNode()}${game.township.resources.filter(resource => resource.itemConversions.length > 0).map(resource => getResourceNode(resource)).join('')}`;
    };

    const updateBiomeList = () => {
        $('#TS_BREAKDOWN_BIOME_LIST').html(getBiomeList());
    };
    const updateTownStatus = () => {
        $('#TS_BREAKDOWN_STATUS_LIST').html(getTownStatus());
    };
    const updateBuildingList = () => {
        $('#TS_BREAKDOWN_BUILDINGS').html(getBuildingList());
    };
    const updateResourceNodes = () => {
        $('#TS_BREAKDOWN_RESOURCES').html(getResourceList());
    };

    const update = () => {
        updateBiomeList();
        updateTownStatus();
        updateBuildingList();
        updateResourceNodes();
    }

    const injectUI = () => {
        const navItem = `
            <li class="nav-main-item">
                <a class="nav-main-link" data-toggle="class-toggle" data-target="#horizontal-navigation-township" data-class="d-none" id="TS_CUSTOM_BTN_BREAKDOWN">
                    <img class="skill-icon-sm m-0 mr-2" src="assets/media/main/settings_header.svg">
                    <span class="nav-main-link-name font-w600">Breakdown</span>
                </a>
            </li>`;
        $('#township-category-menu').find('.nav-main').append(navItem);
        townshipUI.defaultElements.btn.psyBreakdown = document.getElementById('TS_CUSTOM_BTN_BREAKDOWN');
        townshipUI.defaultElements.btn.psyBreakdown.addEventListener('click', () =>{
            townshipUI.showPage(BREAKDOWN_PAGE);
        });

        const breakdownPage = `<div id="TS_CUSTOM_DIV_BREAKDOWN" class="d-none">
            <div class="row no-gutters">
                <div class="col-xl-2 col-lg-4 col-md-12 pr-2 font-size-sm">
                    <div class="border-bottom border-crafting text-warning w-100 pb-1 mb-2 font-size-base">Biomes</div>
                    <div id="TS_BREAKDOWN_BIOME_LIST"></div>
                    <div id="TS_BREAKDOWN_STATUS_LIST"></div>
                    <div class="border-top border-crafting mt-4 pt-4">
                        <button class="btn btn-sm btn-outline-info mt-1 w-100" id="TS_SHOW_MODIFIERS">View Modifier Data</button>
                    </div>
                </div>
                <div class="col-xl-8 col-lg-8 col-md-12 pl-2 pr-2">
                    <div class="border-bottom border-crafting text-warning w-100 pb-1 mb-2 font-size-base">Buildings</div>
                    <div id="TS_BREAKDOWN_BUILDINGS"></div>
                </div>
                <div class="col-xl-2 col-lg-12 col-md-12">
                    <div class="border-bottom border-crafting text-warning w-100 pb-1 mb-2 font-size-base">Resources</div>
                    <ul class="nav-main nav-main-horizontal nav-main-hover nav-main-horizontal-center township font-w400 font-size-sm" id="TS_BREAKDOWN_RESOURCES"></ul>
                </div>
            </div>
        </div>`;
        $('#DIV_CONTAINER').children().first().append(breakdownPage);
        townshipUI.defaultElements.div.psyBreakdown = document.getElementById('TS_CUSTOM_DIV_BREAKDOWN');

        $('#TS_SHOW_MODIFIERS').on('click', () => {
            showModifierData();
        });
    };

    //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    // Game Patches
    ctx.onInterfaceReady(() => {
        injectUI();

        ctx.patch(Township, 'catchupTicks').after(function() {
            if (townshipUI.currentPage == BREAKDOWN_PAGE) {
                updateTownStatus();
                updateResourceNodes();
            }
        });

        ctx.patch(TownshipUI, 'getPageButton').after(function(result, page) {
            if (page == BREAKDOWN_PAGE) {
                return this.defaultElements.btn.psyBreakdown;
            }
            return result;
        });

        ctx.patch(TownshipUI, 'showPage').after(function(result, pageID) {
            this.defaultElements.div.psyBreakdown.classList.add('d-none');
            if (pageID == BREAKDOWN_PAGE) {
                update();
                this.defaultElements.div.psyBreakdown.classList.remove('d-none');
            }
        });

        ctx.patch(TownshipUI, 'displayAllCitizens').replace(function(o) {
            //console.log('Updating Citizens');
            const oldChart = Chart.getChart('populationJobGraph');
            if(oldChart) {
                oldChart.destroy();
            }

            o();
            buildCitizenGraph();
        });
    });
}
